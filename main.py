
# coding: utf-8

# In[16]:


from src.twitch_clip_scraper import GameCategory
import src.twitch_clip_scraper as ttv_scraper
import src.outro as outro
import src.intro as intro
import src.concat as video_maker
from moviepy import *
from moviepy.editor import concatenate_videoclips
import os
import datetime
import random


# In[17]:


def ToSafeFilename(s : str):
    def safe_char(c):
        if c.isalnum():
            return c
        else:
            return "_"
    return "".join(safe_char(c) for c in s).rstrip("_")


# In[20]:


def CreateOutputVideoDirectory(game_category : GameCategory, timestamp = datetime.datetime.now()):
    # Create download directory path
    download_path_list = ["output_videos",
                          ToSafeFilename(game_category.value),
                          timestamp.strftime("%Y"),
                          timestamp.strftime("%m"),
                          timestamp.strftime("%d")]
    download_path_prefix = ""
    dir_ls = os.listdir()
    for path in download_path_list:
        if (download_path_prefix != ""):
            dir_ls = os.listdir(download_path_prefix)
        download_path_prefix += path + "/"
        # Only create directory if it DNE.
        if path not in dir_ls:
            os.mkdir(download_path_prefix)
    print("Created directory: " + download_path_prefix)
    return download_path_prefix


# In[45]:


def GenerateVideoCompilation(game_category : GameCategory, max_clips = 2, num_days = 7):
    """ Main function for running the video compilation generator.
    Overview is as follows:
        1) Scrape top twitch clips from a given category.
        2) Download N of these clips in highest viewing order.
        3) Concatenate and auto edit the videos.
        4) Save the final video output.
    """
    run_timestamp = datetime.datetime.now()
    print("BEGIN TIMESTAMP: ")
    print(run_timestamp)

    # Get clip urls.
    category_url = ttv_scraper.GetTwitchCategoryClipUrl(game_category, num_days)
    clip_url_list = list(ttv_scraper.GetTwitchClipUrls(category_url, max_clips))
    print("CLIP URL LIST LENGTH: " + str(len(clip_url_list)))
    clip_channel_list = [url.split('/')[3] for url in clip_url_list]

    # Randomize order of the clips
    shuffled_url_channels = list(zip(clip_url_list, clip_channel_list))
    random.shuffle(shuffled_url_channels)
    clip_url_list, clip_channel_list = zip(*shuffled_url_channels)
    
    # Generate the input download directory: input_clips/
    # TODO CHANGE INPUT
    download_dir = ttv_scraper.CreateDownloadDirectory("input_clips") + '/'

    # Download the twitch clips into the download directory.
    clip_download_paths = []
    for i, clip_url in enumerate(clip_url_list):
        clip_download_paths.append(ttv_scraper.DownloadTwitchClip(clip_url, download_dir, str(i)))
        print(clip_channel_list[i] + ": " + clip_url)
        print("Download path: " + clip_download_paths[-1])
        print()

    # Compile the video
    edited_video = video_maker.ProcessVideo(clip_download_paths, clip_channel_list)
    outro_clip = outro.OutroGenerator()
    intro_clip = intro.IntroGenerator()
    
    # Download the clip to output_videos/GAME_CATEGORY/YYYY/MM/DD/output.mp4
    output_dir = CreateOutputVideoDirectory(game_category, run_timestamp)
    final_video = concatenate_videoclips([intro_clip, edited_video.crossfadein(1), outro_clip.crossfadein(1)], padding=-1, method='compose')
    final_video.write_videofile(output_dir + '/final.mp4', preset='ultrafast', fps=60)
    run_timestamp = datetime.datetime.now()
    print("END TIMESTAMP: ")
    print(run_timestamp)
    


# In[44]:


#     cookies.append(VideoFileClip('outro.mp4').crossfadein(delay))
#GenerateVideoCompilation(GameCategory.CALL_OF_DUTY_MODERN_WARFARE, max_clips = 17)
GenerateVideoCompilation(GameCategory.VALORANT, max_clips = 17)
#GenerateVideoCompilation(GameCategory.FORTNITE, max_clips = 17)

