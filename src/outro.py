from moviepy.editor import *
import os
import random
import math

def OutroGenerator(music_directory='src/static/songs/', outro_duration=10):
    outro_text = (TextClip("Thanks for watching!", font='src/static/fonts/Gameplay.ttf', fontsize=96, color='white')
            .set_position(('center', 429))
            .set_duration(outro_duration))

    outro_sub = (TextClip("Please subscribe", font='src/static/fonts/Gameplay.ttf', fontsize=46, color='white')
            .set_position(('center', 829), )
            .set_duration(outro_duration))


    song_choice = random.choice(os.listdir(music_directory))
    outro_music = AudioFileClip(music_directory + song_choice)

    song_len = math.floor(outro_music.duration)
    song_start = random.randint(0, song_len - outro_duration)
    song_end = song_start + outro_duration
    outro_music = AudioFileClip(music_directory + song_choice).subclip(song_start, song_end)
    outro_music = outro_music.audio_fadein(4).audio_fadeout(5)
    clip = ColorClip(size=(1920,1080), color=[0,0,0]).set_duration(outro_duration)

    outro_text.audio = outro_music
    return(CompositeVideoClip([clip, outro_text, outro_sub]))
