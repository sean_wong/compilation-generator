from moviepy.editor import *
import os
import gizeh

# Utilize moviepy==1.0.0
# Fix imagemagick.xml permissions
def ProcessVideo(video_paths, channel_names):
    streamer_list = []
    delay = 1 
    cookies = []

    for i, filename in enumerate(video_paths):
        video = VideoFileClip(filename)

        twitch_logo_text = (TextClip(" " + channel_names[i],
                             font='src/static/fonts/Font-Awesome-Brands.otf',
                             fontsize=64,
                             color='white'))
        # Add background for text.
        twitch_logo_text = (twitch_logo_text.on_color(size=(twitch_logo_text.w + 65 ,85),
                                                      color=[0,0,0],
                                                      col_opacity=0.7)
            .set_position((0,0.857), relative=True)
            .set_duration(5))

        result = CompositeVideoClip([video, twitch_logo_text]).resize((1920, 1080))
        cookies.append(result.crossfadein(delay))

    cookie = concatenate_videoclips(cookies, padding=-delay, method='compose')
    return cookie
