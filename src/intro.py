from moviepy.editor import *
import os
import random
from datetime import date, timedelta

def IntroGenerator(intro_duration=2):
    months = {1: "Janurary", 2: "February", 3: "March", 4: "April",
              5: "May", 6: "June", 7: "July", 8: "August",
              9: "September", 10: "October", 11: "November", 12: "December"}

    intro_text = (TextClip("- Weekly Highlight Reel -", font='src/static/fonts/Gameplay.ttf', fontsize=60, color='white')
            .set_position(('center', 429))
            .set_duration(intro_duration))
    
    week_ago = date.today() - timedelta(days=7)

    intro_date = (TextClip(months[week_ago.month] + " " + str(week_ago.day) + " - " + months[date.today().month] + " " + str(date.today().day), font='src/static/fonts/Gameplay.ttf', fontsize=36, color='white')
            .set_position(('center', 829), )
            .set_duration(intro_duration))


    # Create 1920x1080 background.
    clip_frame = ColorClip(size=(1920,1080), color=[0,0,0]).set_duration(intro_duration)

    intro_clip = CompositeVideoClip([clip_frame, intro_text, intro_date])
    intro_clip.fps = 30
    intro_clip.audio = AudioFileClip('shine.wav')
    return(intro_clip)
