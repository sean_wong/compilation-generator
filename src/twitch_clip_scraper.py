
# coding: utf-8

# In[43]:


import urllib
import urllib.request
import os
from bs4 import BeautifulSoup
from selenium import webdriver
import time
from enum import Enum
from selenium.webdriver.chrome.options import Options
import datetime


# In[44]:


class GameCategory(Enum):
    CALL_OF_DUTY_MODERN_WARFARE = "Call of Duty: Modern Warfare"
    JUST_CHATTING = "Just Chatting"
    LEAGUE_OF_LEGENDS = "League of Legends"
    FORTNITE = "Fornite"
    GRAND_THEFT_AUTO_V = "Grand Theft Auto V"
    CSGO = "Counter-Strike: Global Offensive"
    WORLD_OF_WARCRAFT = "World of Warcraft"
    TEAMFIGHT_TACTICS = "Teamfight Tactics"
    VALORANT = "VALORANT"


# In[45]:


def UrlFriendlyStr(string : str):
    """ Returns a URL friendly version of the given string.
    e.g. UrlFriendlyStr("foo:bar cat") --> "foo%3Abar%20cat"
    """
    return urllib.parse.quote(string)


# ### Get Twitch clip links

# In[46]:


def GetTwitchCategoryClipUrl(game : GameCategory, days_back : int):
    """ Constructs the url for the game category clip page. """
    base_url = 'https://www.twitch.tv/directory/game/'
    query_slug = '/clips?range='
    game_slug = UrlFriendlyStr(game.value)
    return base_url + game_slug + query_slug + str(days_back) + 'd'


# In[47]:


def GetPageHtml(url : str):
    """ Returns the html page source of a given url. This is done by opening
    a heavy headless chrome browser to render the page completely.
    """
    chrome_options = Options()  
    chrome_options.add_argument("--headless")  
    browser = webdriver.Chrome(options=chrome_options)
    browser.get(url)
    # Sleep to give time for the page to fully load.
    time.sleep(5)
    html = browser.page_source
    browser.close()
    return html


# In[54]:


def GetTwitchClipUrls(category_url : str, num_clips : int):
    """ Returns a list of clip urls for the given category. The
    clips are ordered from most popular to least popular. This
    property is based on the Twitch's default ordering of clips.
    """
    chrome_options = Options()  
    chrome_options.add_argument("--headless")  
    browser = webdriver.Chrome(options=chrome_options)
    browser.get(category_url)
    # Sleep to give time for the page to fully load.
    time.sleep(5)
    # Click on the language drop down menu.
    button = browser.find_element_by_xpath("//button[@data-test-selector='language-select-menu__toggle-button']")
    button.click()
    time.sleep(5)
    # Click english language button.
    lang_en_button = browser.find_element_by_xpath("//div[@data-language-code='en']")
    lang_en_button.click()
    # Wait for page to reload.
    time.sleep(10)
    html = browser.page_source
    browser.close()

    soup = BeautifulSoup(html, 'html.parser')
    element_list = soup.find_all('a')
    urls = set()
    for e in element_list:
        href = e.get('href')
        # Skip the category clips link at top of page.
        if "directory/game/" in href:
            pass
        elif "clip" in href:
            url = "https://twitch.tv" + href 
            if url not in urls:
                urls.add(url)
        if len(urls) == num_clips:
            break
                
    return urls


# ### Download twitch clips

# In[19]:


def CreateDownloadDirectory(dir_name : str):
    """ Creates a directory for downloading clips. If the directory does not
    exist it is created. Otherwise if the directory does exist, the files
    inside are removed.
    """
    if dir_name in os.listdir():
        for file in os.listdir(dir_name):
            os.remove(dir_name + "/" + file)
    else:
        os.mkdir(dir_name)
    return dir_name


# In[1]:


def DownloadTwitchClip(clip_url : str, download_dir : str, clip_id : str):
    """ Downloads the content from the given url. Downloads the file in
    the specified directory. The file name is of the format: clip_{clip_id}.mp4
    Returns the download path upon success. Empty string if error occurs.
    """
    clip_html = GetPageHtml(clip_url)
    soup = BeautifulSoup(clip_html, 'html.parser')
    bs4_video_download_link = soup.find_all('video')
    if (len(bs4_video_download_link) < 1):
        print("ERROR GETTING DOWNLOAD LINK")
        return ""
    download_link = bs4_video_download_link[0].get('src')
    print(bs4_video_download_link)
    # Download clip.
    download_path = download_dir + "clips_" + clip_id + ".mp4"
    # Overwrite the file if it already exists.
    urllib.request.urlretrieve(download_link, download_path)
    return download_path

